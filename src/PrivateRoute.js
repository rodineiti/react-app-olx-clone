import React from "react";
import { Redirect, Route } from "react-router-dom";
import { isLogged } from "./helpers/auth";

export default ({ children, ...rest }) => {
  const logged = isLogged();
  /**
   * If it is a private page and you are not logged in, block access to the route
   */
  const authorized = rest.private && !logged ? false : true;
  return (
    <Route 
      {...rest}
      render={() => authorized ? children : <Redirect to="/login" />}
    />
  );
};
