import React from "react";
import { Switch } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Profile from "./pages/Profile";
import AdPage from "./pages/AdPage";
import AddAd from "./pages/AddAd";
import EditAd from "./pages/EditAd";
import Ads from "./pages/Ads";
import NotFound from "./pages/NotFound";
import PrivateRoute from "./PrivateRoute";

export default () => {
  return (
    <Switch>
      <PrivateRoute exact path="/">
        <Home />
      </PrivateRoute>
      <PrivateRoute exact path="/login">
        <Login />
      </PrivateRoute>
      <PrivateRoute exact path="/register">
        <Register />
      </PrivateRoute>      
      <PrivateRoute exact path="/profile">
        <Profile />
      </PrivateRoute>      
      <PrivateRoute exact path="/about">
        <About />
      </PrivateRoute>
      <PrivateRoute exact path="/ad/:id">
        <AdPage />
      </PrivateRoute>
      <PrivateRoute exact path="/add-post" private>
        <AddAd />
      </PrivateRoute>
      <PrivateRoute exact path="/edit-post/:id" private>
        <EditAd />
      </PrivateRoute>
      <PrivateRoute exact path="/ads">
        <Ads />
      </PrivateRoute>
      <PrivateRoute>
        <NotFound />
      </PrivateRoute>
    </Switch>
  );
};
