import styled from "styled-components";

export const Template = styled.div``;

export const AppContainer = styled.div`
  max-width: 1000px;
  margin: auto;
`;

export const AppTitle = styled.h1`
  font-size: 27px;
`;

export const AppBody = styled.div``;

export const ErrorMessage = styled.div`
  margin: 10px 0;
  padding: 10px;
  background-color: #ffcaca;
  color: #000;
  border: 2px solid #ff0000;
  border-radius: 3px;
`;
