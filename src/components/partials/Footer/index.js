import React from "react";

import { FooterContainer } from "./styled";

const Footer = () => {
  return (<FooterContainer>
    Todos os direitos reservados <br/>
    OLX Clone
  </FooterContainer>);
};

export default Footer;
