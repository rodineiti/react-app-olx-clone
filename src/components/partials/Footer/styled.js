import styled from "styled-components";

export const FooterContainer = styled.div`
  height: 100px;
  display: flex;
  align-items:center;
  justify-content: center;
  color: #999;
  font-size: 14px;
  text-align: center;
`;
