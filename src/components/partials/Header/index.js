import React from "react";
import { Link } from "react-router-dom";
import { HeaderContainer } from "./styled";

import { isLogged, logout } from "./../../../helpers/auth";

const Header = () => {
  let logged = isLogged();

  const handleLogout = () => {
    logout();
    window.location.href = "/";
  }
  
  return (
    <HeaderContainer>
      <div className="container">
        <div className="logo">
          <Link to="/">
            <span className="logo-1">O</span>
            <span className="logo-2">L</span>
            <span className="logo-3">X</span>
          </Link>
        </div>
        <nav>
          <ul>
            {!logged ? (
              <>
                <li>
                  <Link to="/login">Login</Link>
                </li>
                <li>
                  <Link to="/register">Cadastrar</Link>
                </li>
                <li>
                  <Link to="/add-post" className="btn">
                    Poste um anúncio
                  </Link>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link to="/profile">Minha conta</Link>
                </li>
                <li>
                  <button onClick={handleLogout}>Sair</button>
                </li>
                <li>
                  <Link to="/add-post" className="btn">
                    Poste um anúncio
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    </HeaderContainer>
  );
};

export default Header;
