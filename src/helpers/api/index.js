import Cookies from "js-cookie";
import qs from "qs";
import axios from "axios";

const apiAxios = axios.create({
  baseURL: "http://localhost:8000"
});


const httpAxios = async (method, endpoint, params = {}) => {
  apiAxios.interceptors.request.use(function (config) {
    const token = Cookies.get("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  }, function (err) {
      return Promise.reject(err);
  });

  apiAxios.interceptors.response.use(function (response) {
      return response;
  }, function (err) {
      return Promise.reject(err);
  });

  const config = { headers: { "Content-Type": "multipart/form-data" } };
  let response;

  if (method.toLowerCase() === 'get') {
    response = await apiAxios[method](`${endpoint}?${qs.stringify(params)}`);
  } else if (method.toLowerCase() === 'file') {
    response = await apiAxios.post(endpoint, params, config);
  } else if (method.toLowerCase() === 'post' || method.toLowerCase() === 'put') {
    response = await apiAxios[method](endpoint, params);
  } else {
    response = await apiAxios[method](endpoint);
  }
  
  return response;
}

const api = {
  login: async (email, password) => {
    const json = await httpAxios("post", "/oauth/token", { 
      grant_type: "password",
      client_id: "2",
      client_secret: "2LEi8n6vWThoS1quHrXKBAhCSrEFyDv7DLq4qwlh",
      username: email, 
      password 
    });
    return json;
  },
  register: async (name, email, password, confirmationPassword, uf) => {
    const json = await httpAxios("post", "/api/v1/auth/register", { name, email, password, password_confirmation: confirmationPassword, state: uf });
    return json;
  },
  profile: async () => {
    const json = await httpAxios("get", "/api/v1/auth/me");
    return json;
  },
  updateProfile: async (data) => {
    const json = await httpAxios("post", "/api/v1/auth/updateProfile", data);
    return json;
  },
  getStates: async () => {
    const json = await httpAxios("get", "/api/v1/states");
    return json;
  },
  getCategories: async () => {
    const json = await httpAxios("get", "/api/v1/categories");
    return json;
  },
  getAds: async (params = {}) => {
    const json = await httpAxios("get", "/api/v1/ads", params);
    return json;
  },
  getAd: async (id, extra = false) => {
    const json = await httpAxios("get", "/api/v1/ads/" + id, {
      id, extra
    });
    return json;
  },
  addAd: async (formData) => {
    const json = await httpAxios("file", "/api/v1/ads", formData);
    return json;
  },
  updateAd: async (id, formData) => {
    const json = await httpAxios("file", `/api/v1/ads/${id}/update`, formData);
    return json;
  },
};

export default () => api;
