import Cookies from "js-cookie";

export const isLogged = () => {
  let token = Cookies.get("token");
  return token ? true : false;
};

export const setLogin = (token, remember = false) => {
  if (remember) {
    Cookies.set("token", token, { expires: 999 }); // expires in 999 days
  } else {
    Cookies.set("token", token);
  }
};

export const logout = () => {
  Cookies.remove("token");
};
