import 'react-slideshow-image/dist/styles.css';
import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { Slide } from 'react-slideshow-image';
import { AppContainer } from '../../components/MainComponent';
import AdItem from "../../components/partials/AdItem";
import useApi from '../../helpers/api';
import { AdArea, Fake, OthersArea, BreadChumb } from './styled';

const Login = () => {
  const api = useApi();
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  const [info, setInfo] = useState({});
  
  useEffect(() => {
    async function getAd() {
      const response = await api.getAd(id, true);
      setInfo(response.data);
      setLoading(false);
    }
    getAd();    
  }, [id, api]);

  const formatDate = (date) => {
    let nDate = new Date(date);
    return nDate.toLocaleString('pt-BR', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    });
  };

  return (
    <AppContainer>
      <BreadChumb>
        <Link to="/">Home</Link>
        /
        <Link to={`/ads?uf=${info.user && info.user.state}`}>{info.user && info.user.state}</Link>
        /
        <Link to={`/ads?uf=${info.user && info.user.state}&category=${info.category && info.category.slug}`}>{info.category && info.category.title}</Link>
        / {" "}
        {info.title}
      </BreadChumb>
      <AdArea>
        <div className="left">
          <div className="box">
            <div className="image">
              {loading && <Fake height={300} />}
              {info.images && info.images.length > 0 && (
                <Slide>
                  {info.images.map((image, key) => (
                    <div key={key} className="each-slide">
                      <img src={image.image_url} alt={`${info.title}-${key}`} />
                    </div>
                  ))}
                </Slide>
              )}
            </div>
            <div className="info">
              <div className="name">
                {loading && <Fake height={20} />}
                {info.title && <h2>{info.title}</h2>}
                {info.dateCreated && (
                  <small>Criado em {formatDate(info.dateCreated)}</small>
                )}
              </div>
              <div className="description">
                {loading && <Fake height={100} />}
                {info.description}
                <hr />
                {info.views && <small>Visualizações: {info.views}</small>}
              </div>
            </div>
          </div>
        </div>
        <div className="right">
          <div className="box box--padding">
            {loading && <Fake height={20} />}
            {info.negotiable == 'true' && 'Preço Negociável'}

            {info.negotiable == 'false' && (
              <div className="price">
                Preço:{' '}
                <span>{info.price.toLocaleString('pt-br', {
                  style: 'currency',
                  currency: 'BRL',                  
                })}</span>
              </div>
            )}
          </div>
          {loading && <Fake height={20} />}
          {info.user && (
            <>
              <a href={`mailto:${info.user && info.user.email}`} rel="noopener noreferrer" target="_blank" className="contactSellerInfo">Fale com o vendedor</a>
              <div className="createBy box box--padding">
                <strong>{info.user && info.user.name}</strong>  
                <small>E-mail: {info.user && info.user.name}</small><br/>
                <small>Estado: {info.user && info.user.state}</small><br/>
              </div>
            </>
          )}          
        </div>        
      </AdArea>
      <OthersArea>
        {info.others && (
          <>
            <h2>Outras ofertas do vendedor</h2>
            <div className="list">
            {info.others.map((item, key) => (
              <AdItem  key={key} data={item} />
            ))}
            </div>
          </>
        )}
      </OthersArea>
    </AppContainer>
  );
};

export default Login;
