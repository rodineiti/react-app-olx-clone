import styled from 'styled-components';

export const Fake = styled.div`
  background-color: #ddd;
  height: ${(props) => props.height || 20}px;
`;

export const AdArea = styled.div`
  display: flex;
  margin-top: 20px;

  .box {
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0px 0px 4px #999;
    margin-bottom: 20px;
  }

  .box--padding {
    padding: 10px;
  }

  .left {
    flex: 1;
    margin-right: 20px;

    .box {
      display: flex;
    }

    .image {
      width: 320px;
      height: 320px;
      margin-right: 20px;

      .each-slide img {
        display: flex;
        align-items: center;
        justify-content: center;
        background-size: cover;
        height: 320px;
      }
    }

    .info {
      flex: 1;

      .name {
        margin: 0;
        margin-bottom: 20px;

        h2 {
          margin-top: 20px;
        }

        small {
          color: #ccc;
        }
      }

      .description {
        small {
          color: #ccc;
        }
      }
    }
  }

  .right {
    width: 250px;

    .price span {      
      color:#0000FF;
      display:block;
      font-size:27px;
      font-weight: bold;
    }

    .contactSellerInfo {
      background-color: #0000FF;
      color:#fff;
      height: 30px;
      border-radius:5px;
      box-shadow: 0px 0px 4px #999;
      display: flex;
      justify-content: center;
      align-items: center;
      text-decoration: none;
      margin-bottom: 20px;
    }

    .createBy {
      display: block;
      margin-top: 10px;

      small {
        color:#999;
      }

      strong {
        display: block;
      }
    }
  }

  @media (max-width: 600px) {
    & {
      flex-direction: column;
    }
    .left {
      margin: 0;

      .box {
        width: 320px;
        flex-direction: column;
        margin: auto;
      }

      .info {
        padding: 10px;
      }
    }

    .right {
      width: auto;
      margin-top: 20px;
      
      .box {
        width: 320px;
        margin: auto;
      }

      .contactSellerInfo {
        width: 320px;
        margin: 20px auto;
      }
    }
  }
`;

export const OthersArea = styled.div`
  h2 {
    font-size: 20px;
  }

  .list {
    display:flex;
    flex-wrap: wrap;
    
    .adItem {
      width: 25%;
    }
  }

  @media (max-width: 600px) {
    & {
      margin: 10px;
    }

    .list .adItem {
      width: 50%;
    }
  }
`;

export const BreadChumb = styled.div`
  font-size: 13px;
  margin-top: 20px;

  a {
    display: inline-block;
    margin: 0 5px;
    text-decoration: underline;
    color:#000;
  }

  @media (max-width: 600px) {
    & {
      margin: 20px;
    }
  }
`;
