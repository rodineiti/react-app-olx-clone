import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  AppContainer
} from "../../components/MainComponent";
import AdItem from "../../components/partials/AdItem";
import useApi from "../../helpers/api";
import { AdsArea } from "./styled";
let timer;

const Ads = () => {
  const api = useApi();
  const history = useHistory();
  
  const useQueryString = () => {
    return new URLSearchParams(useLocation().search);
  }
  const query = useQueryString();

  const [term, setTerm] = useState(query.get("term") || "");
  const [uf, setUF] = useState(query.get("uf") || "");
  const [category, setCategory] = useState(query.get("category") || "");

  const [ufsList, setUfsList] = useState([]);
  const [categories, setCategories] = useState([]);
  const [ads, setAds] = useState([]);
  const [opacity, setOpacity] = useState(1);
  const [loading, setLoading] = useState(true);

  const [total, setTotal] = useState(0);
  const [perPage] = useState(9);
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(0);
  
  let pagination = [];
  
  const getAds = async () => {
    setLoading(true);
    const response = await api.getAds({
      limit: perPage,
      q: term,
      cat: category,
      state: uf,
      page
    });
    setAds(response.data.data);
    setTotal(response.data.total);
    setOpacity(1);
    setLoading(false);
  }

  useEffect(() => {
    if (ads.length > 0) {
      setPages(Math.ceil(total / ads.length));
    } else {
      setPages(0);
    }
  }, [total, ads]);

  useEffect(() => {
    setOpacity(0.3);
    getAds();
  }, [page]);
  
  useEffect(() => {
    let query = [];

    if (term) {
      query.push(`term=${term}`);
    }

    if (category) {
      query.push(`category=${category}`);
    }

    if (uf) {
      query.push(`uf=${uf}`);
    }

    history.replace({
      search: `?${query.join("&")}`
    });

    if (timer) {
      clearTimeout(timer);
    }

    // run after 2 seconds
    timer = setTimeout(getAds, 1000);
    setOpacity(0.3);
    setPage(1);
  }, [api, term, category, uf]);

  useEffect(() => {
    async function getStates() {
      const list = await api.getStates();
      setUfsList(list.data);
    }
    getStates();
  }, [api]);

  useEffect(() => {
    async function getCategories() {
      const list = await api.getCategories();
      setCategories(list.data);
    }
    getCategories();
  }, [api]);

  for (let i = 0; i < pages; i++) {
    pagination.push(i + 1);
  }

  console.log(pagination);

  return (
      <AppContainer>
        <AdsArea>
          <div className="left">
            <form action="get">
              <input type="text" name="term" 
              placeholder="Digite aqui sua pesquisa" 
              value={term} onChange={(event) => setTerm(event.target.value)} />
              <div className="filter-state">Estado</div>
              <select name="state" value={uf} onChange={(event) => setUF(event.target.value)}>
                <option value="">Selecione um estado</option>
                {ufsList.length > 0 && ufsList.map((item, key) => (
                  <option key={key} value={item.slug}>{item.slug}</option>
                ))}
              </select>

              <div className="filter-category">Categoria</div>
              <ul>
              {categories.length > 0 && categories.map((item, key) => (
                  <li key={key} 
                      className={category === item.slug ? 'category-item active' : 'category-item'}
                      onClick={() => setCategory(item.slug)}>
                    <img src={item.img} alt={item.title} />
                    <span>{item.title}</span>
                  </li>
                ))}
              </ul>
            </form>
          </div>
          <div className="right">
            <h2>Resultados</h2>
            {loading && ads.length === 0 && <div className="warning">Carregando...</div>}
            {!loading && ads.length === 0 && <div className="warning">Nenhum resultado encontrado</div>}
            <div className="list" style={{opacity: opacity}}>
              {ads.length > 0 && ads.map(item => (
                <AdItem  key={item.id} data={item} />
              ))}
            </div>
            <div className="pagination">
              {pagination.length > 1 && pagination.map((item, key) => (
                <div key={key} onClick={() => setPage(item)} className={item === page ? 'pagination-item active' : 'pagination-item'}>{item}</div>
              ))}
            </div>
          </div>
        </AdsArea>
      </AppContainer>
  );
};

export default Ads;
