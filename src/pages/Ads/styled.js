import styled from "styled-components";

export const AdsArea = styled.div`
  display: flex;
  margin-top: 20px;
  
  .left {
    width: 250px;
    margin-right: 10px;

    .filter-state, .filter-category {
      font-size: 15px;
      margin: 10px 0;
    }

    input, select {
      width: 100%;
      height: 40px;
      border: 2px solid #9bb83c;
      border-radius: 5px;
      outline: 0;
      font-size: 15px;
      color: #000;
      padding: 10px;
    }

    ul, li {
      margin: 0;
      padding: 0;
      list-style: none;
    }

    .category-item {
      display: flex;
      align-items:center;
      padding: 10px;
      border-radius: 5px;
      color:#000;
      cursor:pointer;

      &:hover {
        background-color: #9bb83c;
        color: #fff;  
      }

      &.active {
        background-color: #9bb83c;
        color: #fff;  
      }

      img {
        width: 25px;
        height: 25px;
        margin-right: 5px;
      }

      span {
        font-size:14px;
      }
    }
  }

  .right {
    flex: 1;    

    h2 {
      font-size: 18px;
      margin-top: 0;
    }

    .warning {
      padding: 20px;
      text-align: center;
    }

    .list {
      display: flex;
      flex-wrap: wrap;

      .adItem {
        width: 33.3%;
      }
    }

    .pagination {
      display: flex;
      align-items:center;
      justify-content: center;
      margin: 10px 0;
      flex-wrap: wrap;

      .pagination-item {
        width: 30px;
        height: 30px;
        border:1px solid #000;
        display: flex;
        align-items:center;
        justify-content: center;
        font-size: 14px;
        margin-right: 5px;
        cursor: pointer;

        &:hover {
          border:1px solid #999;
        }

        &.active {
          background-color: #9bb83c;
          border:2px solid #9bb83c;
          color: #fff;
        }
      }
    }
  }

  @media (max-width: 600px) {
    & {
      flex-direction: column;
    }

    .left {
      width: auto;
      margin: 10px;

      ul {
        display: flex;
        flex-wrap: wrap;
      }

      li {
        width: 50%;
      }
    }

    .right {
      margin: 10px;

      .list .adItem {
        width: 50%;
      }
    }
  }
`;
