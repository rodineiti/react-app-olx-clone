import React, { useState, useRef, useEffect } from "react";
import { useParams } from "react-router-dom";
import MaskedInput from "react-text-mask";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import {
  AppContainer,
  AppTitle,
  ErrorMessage
} from "../../components/MainComponent";
import useApi from "../../helpers/api";
import { EditAdArea } from "./styled";

const EditAd = () => {
  const api = useApi();
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [negotiable, setNegotiable] = useState(false);
  const [description, setDescription] = useState("");
  const [error, setError] = useState("");
  const [categories, setCategories] = useState([]);
  const fileField = useRef();

  useEffect(() => {
    async function getAd() {
      const response = await api.getAd(id);
      setTitle(response.data.title);
      setCategory(response.data.category_id);
      setPrice(response.data.price);
      setNegotiable(response.data.negotiable);
      setDescription(response.data.description);
    }
    getAd();    
  }, [id, api]);

  useEffect(() => {
    async function getCategories() {
      const list = await api.getCategories();
      setCategories(list.data);
    }
    getCategories();
  }, [api]);

  const onSubmit = async (e) => {
    e.preventDefault();
    let errors = [];
    setDisabled((prevState) => !disabled);

    if (title.trim() === "") {
      errors.push("Informe o título");
    }

    if (category.trim() === "") {
      errors.push("Informe a categoria");
    }

    if (errors.length === 0) {
      const fData = new FormData();
      fData.append('title', title);
      fData.append('price', price);
      fData.append('negotiable', negotiable ? true : false);
      fData.append('description', description);
      fData.append('category_id', category);

      if(fileField.current.files.length > 0) {
          for(let i=0;i<fileField.current.files.length;i++) {
              fData.append('images[]', fileField.current.files[i]);
          }
      }
      
      const response = await api.updateAd(id, fData);

      if (response.error) {
        setError(response.error);
      } else {
        alert('Atualizado com sucesso');
      }
    } else {
      setError(errors.join("\n"));
    }

    setDisabled(false);
  };

  const priceMask = createNumberMask({
    prefix: "R$ ",
    includeThousandsSeparator: true,
    thousandsSeparatorSymbol: ".",
    allowDecimal: true,
    decimalSymbol: ","
  });

  return (
    <AppContainer>
      <AppTitle>Postar um anúncio</AppTitle>
      <EditAdArea>
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <form onSubmit={onSubmit}>
          <label className="input-label">
            <div className="input--title">Título</div>
            <div className="input--field">
              <input
                type="text"
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Categoria</div>
            <div className="input--field">
              <select
                value={category}
                onChange={(event) => setCategory(event.target.value)}
                disabled={disabled}
                required
              >
                <option value="">Selecione uma categoria</option>
                {categories.length > 0 && categories.map((item, key) => (
                  <option key={key} value={item.id}>{item.title}</option>
                ))}
              </select>
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Preço</div>
            <div className="input--field">
              <MaskedInput
                mask={priceMask}
                placeholder="R$ "
                value={price}
                onChange={(event) => setPrice(event.target.value)}
                disabled={disabled || negotiable}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Descrição</div>
            <div className="input--field">
              <textarea
                onChange={(event) => setDescription(event.target.value)}
                value={description}
                disabled={disabled}
                required
                ></textarea>
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Negociável?</div>
            <div className="input--checkbox">
              <input
                type="checkbox"
                value={negotiable}
                onChange={() => setNegotiable((prevState) => !negotiable)}
                disabled={disabled}
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Imagens (1 ou mais)</div>
            <div className="input--field">
              <input type="file" disabled={disabled} multiple ref={fileField} />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title"></div>
            <div className="input--field">
              <button disabled={disabled}>Postar</button>
            </div>
          </label>
        </form>
      </EditAdArea>
    </AppContainer>
  );
};

export default EditAd;
