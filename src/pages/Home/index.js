import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  AppContainer
} from "../../components/MainComponent";
import AdItem from "../../components/partials/AdItem";
import useApi from "../../helpers/api";
import { HomeArea, SearchArea } from "./styled";

const Home = () => {
  const api = useApi();
  const [uf, setUF] = useState("");
  const [ufsList, setUfsList] = useState([]);
  const [categories, setCategories] = useState([]);
  const [ads, setAds] = useState([]);

  useEffect(() => {
    async function getStates() {
      const list = await api.getStates();
      setUfsList(list.data);
    }
    getStates();
  }, [api]);

  useEffect(() => {
    async function getCategories() {
      const list = await api.getCategories();
      setCategories(list.data);
    }
    getCategories();
  }, [api]);

  useEffect(() => {
    async function getAds() {
      const list = await api.getAds({
        sort: 'desc',
        limit: 8
      });
      setAds(list.data.data);
    }
    getAds();
  }, [api]);

  return (
    <>
      <SearchArea>
        <AppContainer>
          <div className="searchBox">
            <form method="GET" action="/ads">
              <input type="text" name="term" placeholder="O que você procura?" />
              <select
                name="uf"
                type="checkbox"
                value={uf}
                onChange={(event) => setUF(event.target.value)}
              >
                <option value="">Selecione</option>
                {ufsList.length > 0 && ufsList.map(item => (
                  <option key={item.slug} value={item.slug}>{item.slug}</option>
                ))}
              </select>
              <button>Pesquisar</button>
            </form>
          </div>
          <div className="categoryList">
            {categories.length > 0 && categories.map(item => (
              <Link key={item.id} to={`/ads?category=${item.slug}`} className="categoryItem">
                <img alt={item.title} src={item.img} />
                <span>{item.title}</span>
              </Link>
            ))}
          </div>
        </AppContainer>
      </SearchArea>
      <AppContainer>
        <HomeArea>
          <h2>Anúncios recentes</h2>
          <div className="list">
          {ads.length > 0 && ads.map(item => (
              <AdItem  key={item.id} data={item} />
            ))}
          </div>
          <Link to="/ads" className="seeAllAds">Ver todos</Link>
          <hr/>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo ab nemo perferendis asperiores facere numquam sit repellat natus minima maxime? Id doloribus consequatur laborum quaerat nesciunt consequuntur dolorem nobis temporibus.</p>
        </HomeArea>
      </AppContainer>
    </>    
  );
};

export default Home;
