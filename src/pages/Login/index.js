import React, { useState } from "react";
import {
  AppContainer,
  AppTitle,
  ErrorMessage
} from "../../components/MainComponent";
import useApi from "../../helpers/api";
import { setLogin } from "../../helpers/auth";
import { LoginArea } from "./styled";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [remember, setRemember] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [error, setError] = useState("");
  const api = useApi();

  const onSubmit = async (e) => {
    e.preventDefault();
    setDisabled((prevState) => !disabled);

    const response = await api.login(email, password);
    if (response.error) {
      setError(response.error);
    } else {
      setLogin(response.data.access_token, remember);
      window.location.href = "/";
    }

    setDisabled(false);
  };

  return (
    <AppContainer>
      <AppTitle>Login</AppTitle>
      <LoginArea>
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <form onSubmit={onSubmit}>
          <label className="input-label">
            <div className="input--title">E-mail</div>
            <div className="input--field">
              <input
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Senha</div>
            <div className="input--field">
              <input
                type="password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Lembrar senha</div>
            <div className="input--checkbox">
              <input
                type="checkbox"
                value={remember}
                onChange={() => setRemember((prevState) => !remember)}
                disabled={disabled}
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title"></div>
            <div className="input--field">
              <button disabled={disabled}>Acessar</button>
            </div>
          </label>
        </form>
      </LoginArea>
    </AppContainer>
  );
};

export default Login;
