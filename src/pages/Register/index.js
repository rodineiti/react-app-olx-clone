import React, { useState, useEffect } from "react";
import {
  AppContainer,
  AppTitle,
  ErrorMessage
} from "../../components/MainComponent";
import useApi from "../../helpers/api";
import { setLogin } from "../../helpers/auth";
import { RegisterArea } from "./styled";

const Register = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [uf, setUF] = useState("");
  const [password, setPassword] = useState("");
  const [confirmationPassword, setConfirmationPassword] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [error, setError] = useState("");
  const [ufsList, setUfsList] = useState([]);
  const api = useApi();

  useEffect(() => {
    async function getStates() {
      const list = await api.getStates();
      setUfsList(list.data);
    }
    getStates();
  }, [api]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setDisabled((prevState) => !disabled);
    setError("");

    if (password !== confirmationPassword) {
      setError("Senhas não conferem");
      setDisabled(false);
      return;
    }

    const response = await api.register(name, email, password, confirmationPassword, uf);

    if (response.data.error) {
      setError('Erro ao se inscrever, tente novamente.');
    } else {
      setLogin(response.data.access_token);
      window.location.href = "/";
    }

    setDisabled(false);
  };

  return (
    <AppContainer>
      <AppTitle>Cadastro</AppTitle>
      <RegisterArea>
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <form onSubmit={onSubmit}>
          <label className="input-label">
            <div className="input--title">Nome</div>
            <div className="input--field">
              <input
                type="text"
                value={name}
                onChange={(event) => setName(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">E-mail</div>
            <div className="input--field">
              <input
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Senha</div>
            <div className="input--field">
              <input
                type="password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Confirmar Senha</div>
            <div className="input--field">
              <input
                type="password"
                value={confirmationPassword}
                onChange={(event) => setConfirmationPassword(event.target.value)}
                disabled={disabled}
                required
              />
            </div>
          </label>
          <label className="input-label">
            <div className="input--title">Estado</div>
            <div className="input--checkbox">
              <select
                type="checkbox"
                value={uf}
                onChange={(event) => setUF(event.target.value)}
                disabled={disabled}
              >
                <option value="">Selecione</option>
                {ufsList.length > 0 && ufsList.map(item => (
                  <option key={item.slug} value={item.slug}>{item.slug}</option>
                ))}
              </select>
            </div>
          </label>
          <label className="input-label">
            <div className="input--title"></div>
            <div className="input--field">
              <button disabled={disabled}>Criar conta</button>
            </div>
          </label>
        </form>
      </RegisterArea>
    </AppContainer>
  );
};

export default Register;
